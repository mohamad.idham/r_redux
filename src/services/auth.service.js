import axios from 'axios';

const API_URL = 'https://intense-brushlands-12885.herokuapp.com/api/auth/';

const register = (name, username, email, password, passwordConfirmation) => {
  return axios.post(API_URL + 'signup', {
    name,
    username,
    email,
    password,
    passwordConfirmation,
  });
};

const login = (username, password) => {
  return axios
    .post(API_URL + 'signin', {
      username,
      password,
    })
    .then((response) => {
      if (response.data.accessToken) {
        localStorage.setItem('user', JSON.stringify(response.data));
      }

      return response.data;
    });
};

const logout = () => {
  localStorage.removeItem('user');
};

export default {
  register,
  login,
  logout,
};

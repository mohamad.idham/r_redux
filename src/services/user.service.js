import axios from 'axios';
import authHeader from './auth-header';

// const API_URL = 'https://user-article.herokuapp.com/api/';
const API_URL = 'https://intense-brushlands-12885.herokuapp.com/api/';

const getPublicContent = () => {
  return axios.get(API_URL + 'articles');
};

const getArticleDetail = (id) => {
  return axios.get(API_URL + 'articles/' + id);
};

const getUserBoard = () => {
  return axios.get(API_URL + 'users', { headers: authHeader() });
};

const getModeratorBoard = () => {
  return axios.get(API_URL + 'mod', { headers: authHeader() });
};

const getAdminBoard = () => {
  return axios.get(API_URL + 'admin', { headers: authHeader() });
};

export default {
  getPublicContent,
  getArticleDetail,
  getUserBoard,
  getModeratorBoard,
  getAdminBoard,
};

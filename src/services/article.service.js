import axios from 'axios';
import authHeader from './auth-header';

// const API_URL = 'http://172.16.0.205:3000/api/';
const API_URL = 'https://intense-brushlands-12885.herokuapp.com/api/';

const getPublicContent = () => {
  return axios.get(API_URL + 'articles');
};

const getArticleDetail = (id) => {
  return axios.get(API_URL + 'articles/' + id);
};

const postCreateArticle = (title, content) => {
  return axios.post(
    API_URL + 'articles/',
    { title, content },
    { headers: authHeader() }
  );
};

export default {
  getPublicContent,
  getArticleDetail,
  postCreateArticle,
};

import React, { useState, useRef } from 'react';
import { Redirect, useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux';

import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Textarea from 'react-validation/build/textarea';
import CheckButton from 'react-validation/build/button';

import ArticleService from '../../services/article.service';

const required = (value) => {
  if (!value) {
    return (
      <div className="alert alert-danger" role="alert">
        This field is required!
      </div>
    );
  }
};

const CreateArticle = (props) => {
  let history = useHistory();
  const form = useRef();
  const checkBtn = useRef();

  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');
  const [loading, setLoading] = useState(false);

  const { message } = useSelector((state) => state.message);

  const { user: currentUser } = useSelector((state) => state.auth);

  if (!currentUser) {
    return <Redirect to="/login" />;
  }

  const onChangeTitle = (e) => {
    const title = e.target.value;
    setTitle(title);
  };

  const onChangeContent = (e) => {
    const content = e.target.value;
    setContent(content);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    setLoading(true);

    form.current.validateAll();

    if (checkBtn.current.context._errors.length === 0) {
      ArticleService.postCreateArticle(title, content).then(
        (response) => {
          // setContent(response.data);
          alert('Article Successfully Created');
          history.push('/home');
        },
        (error) => {
          const _content =
            (error.response && error.response.data) ||
            error.message ||
            error.toString();

          setContent(_content);
        }
      );
    } else {
      setLoading(false);
    }
  };

  return (
    <div className="col-md-12">
      {/* <div className="card card-container"> */}
        {/* <img
          src="//ssl.gstatic.com/accounts/ui/avatar_2x.png"
          alt="profile-img"
          className="profile-img-card"
        /> */}

        <Form onSubmit={handleSubmit} ref={form}>
          <div className="form-group">
            <label htmlFor="title">Title</label>
            <Input
              type="text"
              className="form-control"
              name="title"
              value={title}
              onChange={onChangeTitle}
              validations={[required]}
            />
          </div>

          <div className="form-group">
            <label htmlFor="content">Content</label>
            <Textarea
              className="form-control"
              name="content"
              rows="8"
              value={content}
              onChange={onChangeContent}
              validations={[required]}
            />
          </div>

          <div className="form-group">
            <button className="btn btn-primary btn-block" disabled={loading}>
              {loading && (
                <span className="spinner-border spinner-border-sm"></span>
              )}
              <span>Submit</span>
            </button>
          </div>

          {message && (
            <div className="form-group">
              <div className="alert alert-danger" role="alert">
                {message}
              </div>
            </div>
          )}
          <CheckButton style={{ display: 'none' }} ref={checkBtn} />
        </Form>
      {/* </div> */}
    </div>
  );
};

export default CreateArticle;

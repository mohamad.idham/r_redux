import React, { useState, useEffect } from 'react';
import Moment from 'moment';

import ArticleService from '../../services/article.service';
import { useParams, useHistory } from 'react-router-dom';

const Details = (props) => {
  let { id } = useParams();
  let history = useHistory();
  const [content, setContent] = useState('');

  useEffect(() => {
    ArticleService.getArticleDetail(id).then(
      (response) => {
        setContent(response.data);
      },
      (error) => {
        const _content =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();

        setContent(_content);
      }
    );
  }, [id]);

  return (
    <div className="container">
      {/* {console.log(content)} */}
      {!content ? (
        <div className="d-flex justify-content-center">
          <div className="spinner-border text-primary" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      ) : (
        <header className="jumbotron">
          <h1 className="mt-4">{content.title}</h1>

          <p className="lead">by {content.author.name}</p>
            <hr />

            <p>Posted on {Moment(content.createdAt).format('d MMMM YYYY')} </p>
            <hr />

            <p className="lead">{content.content}</p>
            <p>
              <a onClick={history.goBack} className="btn btn-info" href="\#">
                Back
              </a>
            </p>
        </header>
      )}
    </div>
  );
};

export default Details;

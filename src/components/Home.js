import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import Moment from 'moment';

import ArticleService from '../services/article.service';
import './Home.css';

const Home = (props) => {
  const [content, setContent] = useState('');
  const { user: currentUser } = useSelector((state) => state.auth);
  const [iseng, setIseng] = useState(true);

  useEffect(() => {
    ArticleService.getPublicContent().then(
      (response) => {
        setContent(response.data);
      },
      (error) => {
        const _content =
          (error.response && error.response.data) ||
          error.message ||
          error.toString();

        setContent(_content);
      }
    );
  }, []);

  const deleteItem = (e) => {
    alert('delete dipencet');
  };

  const updateItem = (e) => {
    alert('update dipencet');
  };

  const kirimIseng = (e) => {
    iseng === true ? setIseng(false) : setIseng(true);
  };

  return (
    <div className="container">
      {console.log(content)}
      {!content.article ? (
        <div className="d-flex justify-content-center mt-5">
          <div className="spinner-border text-primary" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      ) : (
        content.article.map((article) => (
          <header className="jumbotron" key={article.id}>
            <h2 className="mt-4">{article.title}</h2>
            {/* {currentUser.name === article.user.name ? (
              <>
                <a
                  href="\#"
                  className="btn btn-sm btn-warning float-right"
                  onClick={(e) => {
                    if (
                      window.confirm('Are you sure you wish to delete this item?')
                    )
                      deleteItem(e);
                  }}
                >
                  Delete Article
                </a>
                <a
                href="\#"
                className="btn btn-sm btn-info float-right"
                onClick={(e) => {
                  if (
                    window.confirm('Are you sure you wish to update this item?')
                  )
                    deleteItem(e);
                }}
              >
                Update Article
              </a>
              </>
            ) : (
              ''
            )} */}

            <p className="lead">by {article.user.name}</p>
            <hr />

            <p>Posted on {Moment(article.createdAt).format('d MMMM YYYY')} </p>
            <hr />

            <p className="lead">
              {article.content.substring(0, 200)}...{' '}
              <a href={'/articles/details/' + article.id}>read more</a>{' '}
            </p>
          </header>
        ))
      )}

      <button onClick={kirimIseng}>Coba</button>
    </div>
  );
};

export default Home;

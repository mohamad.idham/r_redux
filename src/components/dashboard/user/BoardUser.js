import React, { useState, useEffect } from 'react';

import UserService from './../../../services/user.service';

const BoardUser = () => {
  const [content, setContent] = useState('');

  useEffect(() => {
    UserService.getUserBoard().then(
      (response) => {
        setContent(response.data);
      },
      (error) => {
        const _content =
          (error.response &&
            error.response.data &&
            error.response.data.message) ||
          error.message ||
          error.toString();

        setContent(_content);
      }
    );
  }, []);

  return (
    <div className="container">
      {content.length === 0 ? (
        <div className="d-flex justify-content-center">
          <div className="spinner-border text-primary" role="status">
            <span className="sr-only">Loading...</span>
          </div>
        </div>
      ) : (
        // content.article.map((article) => (
        //   <header className="jumbotron">
        //     <h1 className="mt-4">{article.title}</h1>

        //     <p className="lead">by {article.user.name}</p>
        //     <hr />

        //     <p>Posted on DONI LUPA RETURN "created date"</p>
        //     <hr />

        //     <p class="lead">{article.content}</p>
        //   </header>
        // ))
        <p>{console.log(content)}</p>
      )}
    </div>
  );
};

export default BoardUser;
